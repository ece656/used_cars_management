use UsedCarsDataset;
SET GLOBAL local_infile=1;

-- mysql --local_infile=1 -u root -p


DROP TABLE IF EXISTS Car_OuterSpecs;
CREATE TABLE IF NOT EXISTS Car_OuterSpecs(
vin VARCHAR (30) primary key,
exterior_color VARCHAR (50) NOT NULL,
frame_damaged varchar(5) ,
height DECIMAL(5,2) ,
interior_color VARCHAR (50) NOT NULL,
length DECIMAL(5,2) ,
wheel_system Char(3) DEFAULT NULL,
wheel_system_display varchar(40) Default NULL,
wheelbase DECIMAL(5,2) DEFAULT NULL ,
width  DECIMAL(5,2) DEFAULT NULL
);

load data local infile '/Users/supreetshergill/Documents/Waterloo/ECE656/project/car_outer_specs.csv' ignore into table Car_OuterSpecs
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n';


DROP TABLE IF EXISTS Car_InnerSpecs;
CREATE TABLE IF NOT EXISTS Car_InnerSpecs(
vin VARCHAR (30) primary key,
back_legroom DECIMAL(5,2) DEFAULT NULL,
bed VARCHAR (20) NOT NULL,
bedlength DECIMAL(5,2) DEFAULT Null ,
body_type VARCHAR (50) DEFAULT NULL,
front_legroom DECIMAL(5,2) DEFAULT NULL
) ;

load data local infile '/Users/supreetshergill/Documents/Waterloo/ECE656/project/car_inner_specs.csv' ignore into table Car_InnerSpecs
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n';


DROP TABLE IF EXISTS Car_Fuel;
CREATE TABLE IF NOT EXISTS Car_Fuel(
`vin` VARCHAR (30) primary key,
`city_fuel_economy` INT DEFAULT NULL,
`fuel_tank_volume`DECIMAL(5,2) DEFAULT NULL,
`fuel_type` VARCHAR (20) DEFAULT NULL,
`highway_fuel_economy` INT DEFAULT NULL
) ;

load data local infile '/Users/supreetshergill/Documents/Waterloo/ECE656/project/car_fuel.csv' ignore into table Car_Fuel
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n';


DROP TABLE IF EXISTS User_Car;
DROP TABLE IF EXISTS Car_Make;
DROP TABLE IF EXISTS Car_Engine;
CREATE TABLE IF NOT EXISTS Car_Engine(
`vin` VARCHAR (30) primary key,
`horsepower` INT ,
`engine_cylinders` VARCHAR (50) DEFAULT NULL,
`engine_displacement` INT DEFAULT NULL,
`engine_type` VARCHAR (50) DEFAULT NULL,
`power` VARCHAR (50) DEFAULT 'NA',
`RPM` INT
) ;


load data local infile '/Users/supreetshergill/Documents/Waterloo/ECE656/project/car_engine.csv' ignore into table Car_Engine
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n';

DROP TABLE IF EXISTS Car_Seller;
CREATE TABLE IF NOT EXISTS Car_Seller(
dealerID int primary key,
dealer_zip int ,
seller_rating DECIMAL(5,2) DEFAULT NULL,
franchise_dealer varchar(5) NOT NULL,
franchise_make varchar(60) DEFAULT NULL,
vin VARCHAR (30)
) ;

load data local infile '/Users/supreetshergill/Documents/Waterloo/ECE656/project/car_seller.csv' ignore into table Car_Seller
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n';


DROP TABLE IF EXISTS Car_Listing;
CREATE TABLE IF NOT EXISTS Car_Listing(
listing_id int primary key,
listing_date Date,
city varchar(40),
daysonmarket int,
fleet varchar(5) ,
listing_color varchar(40) Default 'UNKNOWN',
frame_damaged varchar(5) default NULL,
has_accidents varchar(5) default NULL,
isCab varchar(5) default NULL,
is_cpo ENUM('TRUE', 'FALSE') DEFAULT 'FALSE',
ownercount int default NULL,
savingsamount int default 0,
theftitle varchar(5),
vin VARCHAR (30)
);

load data local infile '/Users/supreetshergill/Documents/Waterloo/ECE656/project/car_listing.csv' ignore into table  Car_Listing
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n';



CREATE TABLE IF NOT EXISTS Car_Make(
`vin` VARCHAR (30) primary key,
`make_name` VARCHAR (50) NOT NULL,
`model_name` VARCHAR (50) NOT NULL,
`price` INT NOT NULL check (price > 0),
`sp_name` VARCHAR (80) NOT NULL,
`sp_id` INT NOT NULL,
`trim_id` VARCHAR (20) DEFAULT NULL,
`trim_name` VARCHAR (50) DEFAULT NULL,
`dealerID` int NOT NULL,
`listing_id` int NOT NULL,
 foreign key (dealerID) references Car_Seller(dealerID) ON DELETE CASCADE,
 foreign key(listing_id) references Car_Listing(listing_id) ON DELETE CASCADE
) ;

load data local infile '/Users/supreetshergill/Documents/Waterloo/ECE656/project/car_make.csv' ignore into table Car_Make
     fields terminated by ','
     enclosed by '"'
     lines terminated by '\n';

ALTER TABLE Car_Make ADD isAvailable ENUM('TRUE', 'FALSE') DEFAULT 'TRUE';

CREATE TABLE IF NOT EXISTS User_Car (
`user_id` VARCHAR (30) ,
`vin` VARCHAR (30),
 primary key (user_id, vin),
 foreign key (vin) references Car_Make(vin)
) ;

DROP TRIGGER IF EXISTS delete_zero_engine_data;

DELIMITER $$
CREATE TRIGGER delete_zero_engine_data AFTER INSERT ON Car_Engine FOR EACH ROW
BEGIN
     IF NEW.horsepower = 0 AND NEW.rpm = 0 and NEW.engine_displacement = '' THEN
          DELETE FROM Car_Engine WHERE vin = NEW.vin;
     end IF;
END$$
DELIMITER ;




-- CREATE INDEX search_city_index ON Car_Listing (city);

-- CREATE INDEX search_price_index ON Car_Make (price);




