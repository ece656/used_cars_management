buy_car = "Insert into User_Car(user_id,vin) values ('{}', '{}')"

find_car_by_vin_available = "Select make_name, model_name, price from Car_Make where vin = '{}' and isAvailable = " \
                            "'TRUE' "

find_car_by_make_price = "select vin, make_name, model_name, price from Car_Make where make_name = '{}' and " \
                         "model_name = '{}' " \
                         "and isAvailable = 'TRUE' limit {} "

user_cars_bought = "Select make_name, model_name, price from Car_Make where vin = '{}' "

car_bought_unavailable = "UPDATE Car_Make SET isAvailable = 'FALSE' WHERE vin = '{}'"

cars_for_user = "select vin from User_Car where user_id = '{}'"

search_by_city = "select make_name, model_name, price, city from Car_Make INNER JOIN Car_Listing ON " \
                 "Car_Make.listing_id = Car_Listing.listing_id " \
                 "where city = '{}' and isAvailable = 'TRUE' " \
                 "ORDER BY Car_Make.price ASC limit {}"

search_by_pricerange = "select vin, make_name, model_name, price from Car_Make " \
                       "where price >= {} and price <= {} and isAvailable = 'TRUE' " \
                       "ORDER BY price ASC limit {}"

top_cars_fuel_economy = "SELECT Car_Make.vin, make_name, model_name, price, city_fuel_economy FROM Car_Make INNER JOIN " \
                        "Car_Fuel ON " \
                        "Car_Make.vin = Car_Fuel.vin " \
                        "ORDER BY Car_Fuel.city_fuel_economy DESC limit {}"

top_cars_tank_volume = "SELECT Car_Make.vin, make_name, model_name, price, fuel_tank_volume FROM Car_Make INNER JOIN Car_Fuel " \
                       "ON " \
                       "Car_Make.vin = Car_Fuel.vin " \
                       "ORDER BY Car_Fuel.fuel_tank_volume DESC limit {}"

top_cars_seller_rating = "SELECT Car_Make.vin, make_name, model_name, price, franchise_make, seller_rating FROM Car_Make INNER JOIN " \
                         "Car_Seller ON " \
                         "Car_Make.dealerID = Car_Seller.dealerID " \
                         "ORDER BY Car_Seller.seller_rating DESC limit {}"

top_cars_engine = "SELECT Car_Make.vin, make_name, model_name, price, horsepower FROM Car_Make INNER JOIN Car_Engine ON " \
                  "Car_Make.vin = Car_Engine.vin " \
                  "ORDER BY Car_Engine.horsepower DESC limit {}"
