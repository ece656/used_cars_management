import UserQueries


# Define function to execute user options
def executeUser(cursor, connection):
    print("Welcome User\n")
    # Prompt user for option
    user_option = input("1.Buy a car\n2.Show bought cars\n3.Search cars\n4.See top cars\nEnter Option: ")
    # Call executeOption() function with user's option and database connection parameters
    executeOption(user_option, cursor, connection)


# Define function to execute user's chosen option
def executeOption(option, cursor, connection):
    if option == '1':
        # If user chooses option 1, call buyCar() function with database cursor and connection parameters
        buyCar(cursor, connection)
    elif option == '2':
        # If user chooses option 2, call showUserBoughtCars() function with database cursor parameter
        showUserBoughtCars(cursor)
    elif option == '3':
        # If user chooses option 3, call searchCars() function with database cursor parameter
        searchCars(cursor)
    elif option == '4':
        # If user chooses option 4, call showTopCars() function with database cursor parameter
        showTopCars(cursor)
    else:
        # If user chooses an invalid option, print error message
        print("INVALID OPTION")


# Define function to show cars bought by user
def showUserBoughtCars(cursor):
    # Prompt user for their user ID
    user_id = input("Please enter your user id : ")
    # Execute SQL query to retrieve all cars bought by the user
    cursor.execute(UserQueries.cars_for_user.format(user_id))
    # Fetch all the records returned by the query
    records = cursor.fetchall()
    # If there are records, print details of each car bought by the user
    if records:
        for row in records:
            # Extract VIN from the record
            vin = row[0]
            # Execute SQL query to retrieve make name, model name, and price of the car with the given VIN
            cursor.execute(UserQueries.user_cars_bought.format(vin))
            # Fetch all the records returned by the query
            records1 = cursor.fetchall()
            # Print details of the car
            for r in records1:
                print("Make Name = ", r[0], ", Model Name = ", r[1], ", Price = ", r[2], "$")
    # If there are no records, print error message
    else:
        print("No records available for user id.")


# Define function to search for cars based on user's input
def searchCars(cursor):
    # Prompt user for search option
    search_option = input("1. Search by city\n2. Search by price range\n Enter Option: ")
    # If user chooses option 1, call searchByCity() function
    if search_option == '1':
        searchByCity(cursor)
    # If user chooses option 2, call searchByPriceRange() function
    elif search_option == '2':
        searchByPriceRange(cursor)
    # If user chooses an invalid option, print error message
    else:
        print("INVALID OPTION")


# Define function to search for cars by city
def searchByCity(cursor):
    # Prompt user for city and limit
    city = input("Please enter the city : ")
    limit = input("Please enter limit for results : ")
    # Execute SQL query to retrieve all cars located in the given city
    cursor.execute(UserQueries.search_by_city.format(city, limit))
    # Fetch all the records returned by the query
    records = cursor.fetchall()
    # If there are records, print details of each car
    if records:
        count = 1
        for row in records:
            print(count, ". Make Name = ", row[0], ", Model Name = ", row[1], ", Price = ", row[2], "$ , City = ",
                  row[3])
            count = count + 1
    # If there are no records, print error message
    else:
        print("No cars found for this city")


# This function searches for cars within a specific price range
def searchByPriceRange(cursor):
    # Prompt user to enter lower and higher values of price range, and limit for number of results to be displayed
    lower_range = input("Please enter lower value of range : ")
    higher_range = input("Please enter higher value of range : ")
    limit = input("Please enter limit for results : ")

    # Execute SQL query using the input values and fetch all the records that match the search criteria
    cursor.execute(UserQueries.search_by_pricerange.format(lower_range, higher_range, limit))
    records = cursor.fetchall()

    # If any matching records are found, print them to the console in a specific format
    if records:
        count = 1
        print("Please find the top 10 cars sorted on prices : ")
        for row in records:
            print(count, ". VIN = ", row[0], " Make Name = ", row[1], ", Model Name = ", row[2], ", Price = ", row[3],
                  "$")
            count = count + 1
    # If no matching records are found, inform the user
    else:
        print("No cars found for this price range")


# This function prompts the user to choose an option to display top cars based on certain criteria
def showTopCars(cursor):
    # Prompt the user to choose from a list of options
    search_option = input(
        "Show top cars on the basis of : \n1.Horsepower\n2.Fuel Economy\n3.Fuel Tank Volume\n4.Seller Rating\nEnter "
        "Option: ")

    # If the user chooses option 1, call the function to display top cars based on horsepower
    if search_option == '1':
        topCarsEngine(cursor)
    # If the user chooses option 2, call the function to display top cars based on fuel economy
    elif search_option == '2':
        topCarsFuelEconomy(cursor)
    # If the user chooses option 3, call the function to display top cars based on fuel tank volume
    elif search_option == '3':
        topCarsTankVolume(cursor)
    # If the user chooses option 4, call the function to display top cars based on seller rating
    elif search_option == '4':
        topCarsSellerRating(cursor)
    # If the user chooses an invalid option, inform them that it is not a valid choice
    else:
        print("INVALID OPTION")


# This function displays the top cars based on fuel economy
def topCarsFuelEconomy(cursor):
    # Prompt the user to enter a limit for the number of results to be displayed
    limit = input("Please enter the limit : ")

    # Execute SQL query to retrieve the top cars based on fuel economy and fetch all matching records
    cursor.execute(UserQueries.top_cars_fuel_economy.format(limit))
    records = cursor.fetchall()

    # Loop through the records and print each one to the console in a specific format
    count = 1
    for row in records:
        print(count, ". VIN = ", row[0], " Make Name = ", row[1], ", Model Name = ", row[2], ", Price = ", row[3],
              "$ , Economy = ", row[4])
        count = count + 1


# This function displays the top cars based on fuel tank volume
def topCarsTankVolume(cursor):
    # Prompt the user to enter a limit for the number of results to be displayed
    limit = input("Please enter the limit : ")

    # Execute SQL query to retrieve the top cars based on fuel tank volume and fetch all matching records
    cursor.execute(UserQueries.top_cars_tank_volume.format(limit))
    records = cursor.fetchall()

    # Loop through the records and print each one to the console in a specific format
    count = 1
    for row in records:
        print(count, ". VIN = ", row[0], " Make Name = ", row[1], ", Model Name = ", row[2], ", Price = ", row[3],
              "$ , Volume = ", row[4])
        count = count + 1


# This function displays the top cars based on seller rating
def topCarsSellerRating(cursor):
    # Prompt the user to enter a limit for the number of results to be displayed
    limit = input("Please enter the limit : ")

    # Execute SQL query to retrieve the top cars based on seller rating and fetch all matching records
    cursor.execute(UserQueries.top_cars_seller_rating.format(limit))
    records = cursor.fetchall()

    # Loop through the records and print each one to the console in a specific format
    count = 1
    for row in records:
        print(count, ". VIN = ", row[0], " Make Name = ", row[1], ", Model Name = ", row[2], ", Price = ", row[3],
              "$ , Seller = ", row[4], ", Seller Rating = ", row[5])
        count = count + 1


# This function displays the top cars based on engine horsepower
def topCarsEngine(cursor):
    # Prompt the user to enter a limit for the number of results to be displayed
    limit = input("Please enter the limit : ")

    # Execute SQL query to retrieve the top cars based on engine horsepower and fetch all matching records
    cursor.execute(UserQueries.top_cars_engine.format(limit))
    records = cursor.fetchall()

    # Loop through the records and print each one to the console in a specific format
    count = 1
    for row in records:
        print(count, ". VIN = ", row[0], " Make Name = ", row[1], ", Model Name = ", row[2], ", Price = ", row[3],
              "$ , Horsepower = ", row[4])
        count = count + 1


def buyCar(cursor, connection):
    # Ask for user ID to associate with the purchase
    user_id = input("Please enter your user id : ")

    # Ask for the search option
    choice = input("Select an option : \n1.Search your car\n2.You know the VIN\n Enter your choice : ")

    # Initialize VIN to 0
    vin = 0

    # If search option is 1, ask for make, model, and limit
    if choice == '1':
        makename = input("Enter make name of the car : ")
        modelname = input("Enter model name of the car : ")
        limit = input("Please enter the limit for results : ")

        # Execute the query to find cars by make and model within the given price limit
        cursor.execute(UserQueries.find_car_by_make_price.format(makename, modelname, limit))
        cars = cursor.fetchall()

        # If cars are found, print their details
        if cars:
            for row in cars:
                print("VIN = ", row[0], ", Make Name = ", row[1], ", Model Name = ", row[2], ", Price = ", row[3], "$ ")
        else:
            # If no cars are found, print a message
            print("No cars found")

        # Ask for the VIN of the car to buy
        vin = input("Please enter the vehicle identification number : ")
    elif choice == '2':
        # If search option is 2, ask for VIN
        vin = input("Please enter the vehicle identification number : ")
    else:
        # If an invalid search option is entered, print an error message
        print("INVALID OPTION")

    # Execute the query to find the car with the given VIN that is available
    cursor.execute(UserQueries.find_car_by_vin_available.format(vin))
    records = cursor.fetchall()

    # If a car is found, proceed with the purchase
    if records:

        # Execute the query to mark the car as bought by the user
        cursor.execute(UserQueries.buy_car.format(user_id, vin))
        connection.commit()

        # Execute the query to mark the car as unavailable for purchase
        cursor.execute(UserQueries.car_bought_unavailable.format(vin))
        connection.commit()

        # Print a message to confirm the purchase and print the car details
        row = records[0]
        print("Congratulations you have bought the car ")
        print("Car Details :")
        print("Make Name = ", row[0], ", Model Name = ", row[1], ", Price = ", row[2])

    else:
        # If no car is found or the car is not available, print an error message
        print("Entered vin does not match any car or the car is not available.")
