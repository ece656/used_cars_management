# Importing the necessary modules
import mysql.connector
from UserService import executeUser
from AdminService import executeAdmin


# Defining the main function
def main():
    try:
        # Creating a connection to the MySQL server with the specified credentials
        connection = mysql.connector.connect(host='localhost',
                                             database='UsedCarsDataset',
                                             user='root',
                                             password='your_password')

        # Creating a cursor object to execute SQL queries on the database
        cursor = connection.cursor()

        # Allowing the user to select between two options: User or Admin
        login_option = input("Login as.\n1.User\n2.Admin\nEnter Option: ")

        # If the user selects '1', call the executeUser function from the UserService module
        if login_option == '1':
            executeUser(cursor, connection)

        # If the user selects '2', call the executeAdmin function from the AdminService module
        elif login_option == '2':
            executeAdmin(cursor, connection)

        # If the user selects anything else, print an error message
        else:
            print("Invalid option")

    # Catching any MySQL errors that may occur and printing them
    except mysql.connector.Error as e:
        print("Error reading data from MySQL table", e)

    # Closing the database connection and cursor
    finally:
        if connection.is_connected():
            connection.close()
            cursor.close()
            print("MySQL connection is closed")


# Calling the main function if the script is being run as the main program
if __name__ == "__main__":
    main()
