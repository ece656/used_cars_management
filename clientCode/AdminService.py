import AdminQueries


# This function is used to execute various options available to the admin.
# It takes two arguments, a cursor and a connection.
def executeAdmin(cursor, connection):
    print("Welcome Admin\n")

    # Ask the admin to select an option from the available ones.
    user_option = input("1.View tables\n2.Delete a record\nEnter Option: ")

    # Call the executeOption function to carry out the selected option.
    executeOption(user_option, cursor, connection)


# This function is used to carry out the option selected by the admin.
# It takes three arguments, the selected option, a cursor and a connection.
def executeOption(option, cursor, connection):
    # Check which option was selected and call the appropriate function.
    if option == '1':
        viewTables(cursor)
    elif option == '2':
        deleteRecord(cursor, connection)
    else:
        # If an invalid option was selected, print a message to let the admin know.
        print("INVALID OPTION")


# This function is used to delete a record from the database.
# It takes two arguments, a cursor and a connection.
def deleteRecord(cursor, connection):
    # Ask the admin to enter the dealer ID and the listing ID of the record to be deleted.
    dealer = input("Enter your dealer ID : ")
    listing_id = input("Enter the listing id to be removed : ")

    # Execute a query to retrieve the VIN for the given listing ID.
    cursor.execute(AdminQueries.vin_by_listing.format(listing_id))
    record = cursor.fetchone()

    # If a record with the given listing ID is found, delete all related data from the database.
    if record:
        vin = record[0]
        cursor.execute(AdminQueries.delete_car_outer_specs.format(vin))
        connection.commit()
        cursor.execute(AdminQueries.delete_car_inner_specs.format(vin))
        connection.commit()
        cursor.execute(AdminQueries.delete_car_engine.format(vin))
        connection.commit()
        cursor.execute(AdminQueries.delete_car_fuel.format(vin))
        connection.commit()
        cursor.execute(AdminQueries.delete_car_make.format(vin))
        connection.commit()
        cursor.execute(AdminQueries.delete_dealer.format(dealer))
        connection.commit()
        cursor.execute(AdminQueries.delete_listing.format(listing_id))
        connection.commit()

        # Print a message to let the admin know that the record has been successfully deleted.
        print("Given record has been deleted.")
    else:
        # If no record with the given listing ID is found, print a message to let the admin know.
        print("No vehicle for the listing")


# This function is used to view tables in the database and view records from a selected table.
# It takes one argument, a cursor.
def viewTables(cursor):
    # Execute a query to retrieve the names of all tables in the database.
    sql_select_Query = "show tables"
    cursor.execute(sql_select_Query)

    # Fetch all the records returned by the query.
    records = cursor.fetchall()

    # Print the name of each table in the database.
    for row in records:
        print("Table Name = ", row[0])

    # Ask the admin to enter the name of the table they want to view records from, and the maximum number of records
    # to display.
    table_name = input("Enter Table Name to view: ")
    limit = input("limit: ")

    # Execute a query to retrieve all records from the selected table, limited to the specified number.
    sql_select_Query3 = "select * from {} limit {}".format(table_name, limit)
    cursor.execute(sql_select_Query3)
    records = cursor.fetchall()

    # Print the column names for the selected table.
    print(cursor.column_names)

    # Print each record returned by the query.
    for row in records:
        print(row)

