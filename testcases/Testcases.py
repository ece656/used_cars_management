import unittest

from clientCode import Utils
from clientCode import  UserQueries


class UserTest(unittest.TestCase):

    def test_cars_bought_by_user(self):
        content = Utils.db_read(UserQueries.cars_for_user.format('sup647'))
        self.assertEqual(content[0].get("vin"), '1FA6P8AM4G5335237')

    def test_search_car_by_city(self):
        content = Utils.db_read(UserQueries.search_by_city.format('Bridgewater', 1))
        self.assertEqual(content[0].get("make_name"), 'Saturn')
        self.assertEqual(content[0].get("model_name"), 'S-Series')
        self.assertEqual(content[0].get("city"), 'Bridgewater')

    def test_search_by_price(self):
        content = Utils.db_read(UserQueries.search_by_pricerange.format(15000, 16000, 2))
        self.assertEqual(content[0].get("make_name"), 'Jeep')
        self.assertEqual(content[0].get("model_name"), 'Cherokee')

    def test_top_cars_by_power(self):
        content = Utils.db_read(UserQueries.top_cars_engine.format(1))
        self.assertEqual(content[0].get("make_name"), 'Dodge')
        self.assertEqual(content[0].get("model_name"), 'Challenger')

    def test_top_cars_by_volume(self):
        content = Utils.db_read(UserQueries.top_cars_tank_volume.format(1))
        self.assertEqual(content[0].get("make_name"), 'Chevrolet')
        self.assertEqual(content[0].get("model_name"), 'Silverado 3500HD Chassis')

    def test_top_cars_by_rating(self):
        content = Utils.db_read(UserQueries.top_cars_seller_rating.format(1))
        self.assertEqual(content[0].get("make_name"), 'Chevrolet')
        self.assertEqual(content[0].get("model_name"), 'Spark')

    def test_top_cars_fuel_economy(self):
        content = Utils.db_read(UserQueries.top_cars_fuel_economy.format(1))
        print(content)
        self.assertEqual(content[0].get("make_name"), 'BMW')
        self.assertEqual(content[0].get("model_name"), 'i3')








