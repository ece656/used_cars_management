The project aims to design and implement a database system for managing a used cars dataset. The dataset will contain information about used cars such as make, model, year, price, mileage, and other relevant details. The database system will be used to store, manage and retrieve the data efficiently.

Objective:
The main objective of this project is to design and implement a database system for managing a used cars dataset. The system will be used to store, manage and retrieve the data efficiently.
